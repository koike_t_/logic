<?php

// $array = [
//   "taro" => 1,
//   "jiro" => null,
//   "saburo" => "宿題しました。",
//   "shiro" => "",
//   "goro" => "0",
//   "rokuro" => 0,
//   "nanaro" => null,
//   "hachiro" => null,
//   "ichiro" => "1"
// ];

// $absentStudents = [];

// foreach ($array as $key => $value) {
//   if ($value == 1 || $value === '宿題しました。') {
//     $value = true;
//   }
//   if ($value == '0' || $value === '') {
//     $value = false;
//   }
//   if ($value === null) {
//     unset($array[$key]);
//     array_push($absentStudents, $value);
//   }
//   $array[$key] = $value;
// }
// var_dump($array);
// var_dump($absentStudents);

$array = ["taro" => 1, "jiro" => null, "saburo" => "宿題しました。", "shiro" => "", "goro" => "0", "rokuro" => 0, "nanaro" => null, "hachiro" => null, "ichiro" => "1"];

$absentPeople = "";

foreach($array as $key => $value) {
  if ($value) {
    $array[$key] = true;
  } else {
    $array[$key] = false;
  }
  if (intval($value === 1)) {
    $array[$key] = true;
  }
  if (intval($value === 0)) {
    $array[$key] = false;
  }
  if (is_null($value)) {
    unset($array[$key]);
    $absentPeople .= $key.",";
  } 
}