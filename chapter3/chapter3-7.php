<?php

$array = [
  [15, 36, 77],
  [83, 51, 92],
  [28, 64, 10]
];

$i = 0;
$s = 0;

foreach ($array as $values) {
  $i++;
  foreach ($values as $value) {
    $s++;
    echo $i.'番目の配列の'.$s.'番目は'.$value.'です'."\n";
  }
}
// 回答
// $arrayNumbers = [[15, 36, 77],[83, 51, 92],[28, 64, 10]];

// foreach($arrayNumbers AS $key1 => $numbers){
//   foreach($numbers AS $key2 => $number){
//     echo ($key1 + 1)."番目の配列の".($key2 + 1)."番目の要素は".$number."です。\n";
//   }
// }