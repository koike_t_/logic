<?php

$array = [
  ["name" => "Tom", "age" => "20", "country" => "USA"],
  ["name" => "John", "age" => "25", "country" => "France"],
  ["name" => "Lee", "age" => "23", "country" => "China"]
];

foreach ($array as $keys => $values) {

  $values['age'] += 1;
  if ($values['country'] === 'USA') {
    $values['country'] = 'United State of America';
  }
  $array[$keys] = $values;
  print_r($array);
}