<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/style.css">
    <title>5-11</title>
  </head>
  <body>
    <header><h1>Adseed Books</h1></header>
    <div class="container">
      <div class="content">
        <form action="#" method="post">
          <h2>投稿フォーム</h2>
          <table>
            <tr>
              <th>タイトル：</th>
              <td><input type="text" name="title" size="30"></td>
            </tr>
            <tr>
              <th>著者：</th>
              <td><input type="text" name="author" size="30"></td>
            </tr>
            <tr>
              <th>ジャンル</th>
              <td>
                <input type="checkbox" name="genre" value="novel">小説・文学
                <input type="checkbox" name="genre" value="Non-Fiction">ノンフィクション
                <input type="checkbox" name="genre" value="mistery">ミステリー
                <input type="checkbox" name="genre" value="history">歴史
                <input type="checkbox" name="genre" value="bisiness">ビジネス
              </td>
            </tr>
            <tr>
              <th>オススメ度：</th>
              <td>
                <select name="recommend">
                  <option value="1">★</option>
                  <option value="2">★★</option>
                  <option value="3">★★★</option>
                  <option value="4">★★★★</option>
                  <option value="5">★★★★★</option>
                </select>
              </td>
            </tr>
            <tr>
              <th>匿名投稿：</th>
              <td>
                <input type="radio" name="postHide" value="1" checked="checked">匿名で投稿
                <input type="radio" name="postHide" value="0">ニックネームで投稿
              </td>
            </tr>
            <tr>
              <th colspan="2">
                <input type="submit" value="送信">
              </th>
            </tr>
          </table>
        </form>  
      </div>
    </div>
    <footer></footer>
  </body>
</html>
