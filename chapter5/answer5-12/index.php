<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="css/style.css">
	<title>5-12</title>
</head>

<body>
	<header>
		<h1>Adseed Books</h1>
		<div id="nav">
			<ul>
				<li><a href="#">トップ</a></li>
				<li><a href="#">本一覧</a></li>
				<li><a href="#">マイページ</a></li>
			</ul>
		</div>
	</header>
	<div id="container">
		<div class="content">
			<article class="wrapTopImage">
				<img class="topImage" src="images/top_book.jpg">
			</article>
		</div>
		<div class="content">
			<article class="wrapBoxs">
				<div class="box">
					<p>本を読んで知識を広げよう。日常生活のヒントがもらえるかもしれません。</p>
				</div>
				<div class="box"><img src="images/book_with_glass.jpg"></div>
				<div class="box"><img src="images/reading.jpg"></div>
				<div class="box">
					<p>読書にはストレス解消効果があります。音楽鑑賞やお茶など他のストレス解消法を上回るそうです。</p>
				</div>
			</article>
		</div>
		<div class="content wrapBookAndForm">
			<article class="wrapBooks">
      <h2>ブックレビュー</h2>
      <?php $books = [
              ["id" => 1, "title" => "吾輩は猫である", "author" => "夏目漱石", "yearPublished" => "1911"],
              ["id" => 2, "title" => "こころ", "author" => "夏目漱石", "yearPublished" => "1927"],
              ["id" => 3, "title" => "坊ちゃん", "author" => "夏目漱石", "yearPublished" => "1950"],
      ];?>
				<table>
					<tbody>
          <?php foreach($books AS $book) { ?>
						<tr>
							<th >タイトル:</th>
							<td class="title" colspan="3"><?php echo $book["title"]; ?></td>
						</tr>
						<tr>
							<th>著者:</th>
							<td><?php echo $book["author"]; ?></td>
							<th>発表年:</th>
							<td><?php echo $book["yearPublished"]; ?></td>
            </tr>
            <?php } ?>
					</tbody>
				</table>
			</article>
			<form action="cgi-bin/aaa.cgi" method="post">
				<h2>投稿フォーム</h2>
				<table>
					<tbody>
						<tr>
							<th>タイトル：</th>
							<td><input type="text" name="title" size="30"></td>
						</tr>
						<tr>
							<th>著者：</th>
							<td><input type="text" name="author" size="30"></td>
						</tr>
						<tr>
							<th>ジャンル</th>
							<td>
								<input type="checkbox" name="genre" value="novel">小説・文学
								<input type="checkbox" name="genre" value="Non-Fiction">ノンフィクション
								<input type="checkbox" name="genre" value="mistery">ミステリー
								<input type="checkbox" name="genre" value="history">歴史
								<input type="checkbox" name="genre" value="bisiness">ビジネス
							</td>
						</tr>
						<tr>
							<th>オススメ度：</th>
							<td>
								<select name="recommend">
									<option value="1">★</option>
									<option value="2">★★</option>
									<option value="3">★★★</option>
									<option value="4">★★★★</option>
									<option value="5">★★★★★</option>
								</select>
							</td>
						</tr>
						<tr>
							<th>匿名投稿：</th>
							<td>
								<input type="radio" name="postHide" value="1" checked="checked">匿名で投稿
								<input type="radio" name="postHide" value="0">ニックネームで投稿
							</td>
						</tr>
						<tr>
							<th colspan="2">
								<input type="submit" value="送信">
							</th>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
	</div>
	<footer><p>Copyright ©︎2016 Adseed,inc. All right reserved.</p></footer>
</body>

</html>