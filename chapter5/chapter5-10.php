<?php
$books = [
  ["id" => 1, "title" => "吾輩は猫である", "author" => "夏目漱石", "yearPublished" => "1911"],
  ["id" => 2, "title" => "こころ", "author" => "夏目漱石", "yearPublished" => "1927"],
  ["id" => 3, "title" => "坊ちゃん", "author" => "夏目漱石", "yearPublished" => "1950"],
];
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="chapter5-9.css">
  <title>5-9</title>
</head>

<body>
  <header>
    <h1>Adseed Books</h1>
    <header>
      <div class="container">
        <div class="content">
          <article>
            <h2>新着レビュー</h2>
            <table>
              <thead>
                <tr>
                  <th>タイトル</th>
                  <th>著者</th>
                  <th>出版年</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($books as $book): ?>
                <tr>
                  <td><?php echo ($book['title']); ?></td>
                  <td><?php echo ($book['author']); ?></td>
                  <td><?php echo ($book['yearPublished']); ?></td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </article>
        </div>
      </div>
      <footer></footer>
</body>

</html>