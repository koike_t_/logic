<?php
  $books = [
    ["id" => 1, "title" => "吾輩は猫である", "author" => "夏目漱石", "yearPublished" => "1911"],
    ["id" => 2, "title" => "こころ", "author" => "夏目漱石", "yearPublished" => "1927"],
    ["id" => 3, "title" => "坊ちゃん", "author" => "夏目漱石", "yearPublished" => "1950"],
  ];
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="chapter5-12.css">
  <title>Document</title>
</head>

<body>
  <header>
    <h1>Adseed Books</h1>
    <nav class="top-bar">
      <ul>
        <li><a href="#">トップページ</a></li>
        <li><a href="#">本一覧</a></li>
        <li><a href="#">マイページ</a></li>
      </ul>
    </nav>
  </header>
  <main>
    <section class="content">
      <article class="top-image-wrap">
        <img src="./images/top_book.jpg" alt="top-image" class="top-image">
      </article>
    </section>

    <section class="content">
      <div class="box-wrap">
        <div class="box">
          <p>本を読んで知識を広げよう。日常生活のヒントがもらえるかもしれません。</p>
        </div>
        <div class="box">
          <img src="./images/book_with_glass.jpg" alt="book_with_glass-image">
        </div>
        <div class="box">
          <img src="./images/reading.jpg" alt="reading-image">
        </div>
        <div class="box">
          <p>読書にはストレス解消効果があります。音楽鑑賞やお茶など他のストレス解消法を上回るそうです。</p>
        </div>
      </div>
    </section>
    <section class="content book-form-wrap">
      <article class="book-review-wrap">
        <h2>ブックレビュー</h2>
        <table>
          <tbody>
            <?php foreach ($books as $book): ?>
            <tr>
              <th >タイトル:</th>
              <td class="title" colspan="3"><?php echo($book['title']); ?></td>
            </tr>
            <tr>
              <th>著者:</th>
              <td><?php echo($book['author']); ?></td>
              <th>発表年:</th>
              <td><?php echo($book['yearPublished']); ?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </article>
      <form action="#" method="post">
        <h2>投稿フォーム</h2>
        <table>
          <tbody>
            <tr>
              <th>タイトル：</th>
              <td><input type="text" name="title" size="30"></td>
            </tr>
            <tr>
              <th>著者：</th>
              <td><input type="text" name="author" size="30"></td>
            </tr>
            <tr>
              <th>ジャンル</th>
              <td>
                <input type="checkbox" name="genre" value="novel">小説・文学
                <input type="checkbox" name="genre" value="nonFiction">ノンフィクション
                <input type="checkbox" name="genre" value="mystery">ミステリー
                <input type="checkbox" name="genre" value="history">歴史
                <input type="checkbox" name="genre" value="business">ビジネス
              </td>
            </tr>
            <tr>
              <th>オススメ度：</th>
              <td>
                <select name="recommend">
                  <option value="1">★</option>
                  <option value="2">★★</option>
                  <option value="3">★★★</option>
                  <option value="4">★★★★</option>
                  <option value="5">★★★★★</option>
                </select>
              </td>
            </tr>
            <tr>
              <th>匿名投稿：</th>
              <td>
                <input type="radio" name="post-hide" value="1" checked>匿名で投稿
                <input type="radio" name="post-hide" value="2">ニックネームで投稿
              </td>
            </tr>
            <tr>
              <th colspan="2">
                <input type="submit" value="送信">
              </th>
            </tr>
          </tbody>
        </table>
      </form>
    </section>
  </main>
  <footer>
    <p>Copyright ©︎2016 Adseed,inc. All right reserved.</p>
  </footer>
</body>

</html>