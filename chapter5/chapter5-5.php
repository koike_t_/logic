<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="chapter5-5.css">
  <title>Document</title>
</head>

<body>
  <div class="inner">
    <div class="box-top"></div>
    <div class="box-top"></div>
  </div>
  <div class="inner">
    <div class="box-middle"></div>
    <div class="box-middle"></div>
    <div class="box-middle"></div>
  </div>
  <div class="inner">
    <div class="box-bottom"></div>
    <div class="box-bottom"></div>
    <div class="box-bottom"></div>
    <div class="box-bottom"></div>
  </div>
</body>

</html>