<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="chapter5-8.css">
  <title>Document</title>
</head>
<body>
<header>
  <h1>Adseed Books</h1>
</header>
<section>
  <div class="content">
    <img src="./images/top_book.jpg" alt="top-image" class="top-image">
  </div>
  <div class="content">
    <article>
      <h2>Book Review</h2>
      <table>
        <thead>
          <tr>
            <th>タイトル</th>
            <th>著者</th>
            <th>出版年</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>吾輩は猫である</td>
            <td>夏目漱石</td>
            <td>1911</td>
          </tr>
          <tr>
            <td>こころ</td>
            <td>夏目漱石</td>
            <td>1927</td>
          </tr>
          <tr>
            <td>坊ちゃん</td>
            <td>夏目漱石</td>
            <td>1950</td>
          </tr>
        </tbody>
      </table>
    </article>
  </div>
</section>
<footer>
  
</footer>
</body>
</html>