<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="chapter5-9.css">
  <title>5-9</title>
</head>

<body>
  <header>
    <h1>Adseed Books</h1>
    <header>
      <div class="container">
        <div class="content">
          <article>
            <h2>新着レビュー</h2>
            <p>昨日のレビュー数：<?php echo $preWeekCount = 90; ?>件</p>
            <p>今日のレビュー数：<?php echo $thisWeekCount = 100; ?>件</p>
            <p>新着レビュー数：<?php echo $thisWeekCount - $preWeekCount ?>件</p>
          </article>
        </div>
      </div>
      <footer></footer>
</body>

</html>