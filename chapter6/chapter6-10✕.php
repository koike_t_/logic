<?php

$startTime = "09:30";
$endTime = "18:45";

$startTotalMin = timeToMin($startTime);
$endTotalMin = timeToMin($endTime);

$workTotalMin = $endTotalMin - $startTotalMin;

$workHour = floor($workTotalMin / 60);
$workMin = floor($workTotalMin % 60);

echo formatNumToTime($workHour).":".formatNumToTime($workMin)."\n";

function timeToMin($time) {
  $arrayTime = explode(':', $time);
  $hour = intval($arrayTime[0]);
  $min = intval($arrayTime[1]);

  $totalMin = $hour * 60 + $min;
  return $totalMin;
}

function formatNumToTime($num) {
  $strNum = strval($num);
  if ($num <= 9) {
    $strNum = "0".$strNum;
  }

  return $strNum;
}
