<?php 
  $n = 2;
  $m = 5;

  $breadMoney = 500 * $n;
  $halfBreadMoney = 300 * $m;
  $tax = 1.08;

  $calc = ($breadMoney + $halfBreadMoney) * $tax;

  echo number_format($calc).'円';