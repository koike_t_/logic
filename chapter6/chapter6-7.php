<?php 

$totalPrice = 1250;
$point = 240;

$fraction = substr($totalPrice, -3, 3);
if ($point >= $fraction ) {
  $point -= $fraction;
  $totalPrice -= $fraction;
} 

echo  (number_format($totalPrice)).'円'."\n";
echo ($point).'ポイント';
